function calcula() {
    var alturaParede = document.getElementById("alturaParede").value;
    var larguraParede = document.getElementById("larguraParede").value;
    var janelaOuPorta = document.getElementById("portas&janelas").value;

    var adicionaAltura = document.getElementById("altura");
    var adicionaLargura = document.getElementById("largura");
    var adicionarArea = document.getElementById("area");
    var adicionarTinta = document.getElementById("tinta");
    var latasUsadas = {
        "litrosUsados" : QntdTinta,
        "litros18" : 0,
        "litros3" : 0,
        "litros2" : 0,
        "litros0" : 0
    }


    var areaJanela = 2 * 1.2;
    var areaPorta = 1.9 * 0.8;
    var areaParede = alturaParede * larguraParede;


    /////////////////////////////////////
    ///          VALIDAÇÃO            ///
    /////////////////////////////////////


    if (areaParede <= 15 && areaParede >= 1) {
        if (janelaOuPorta == "nao") {
            var QntdTinta = Math.ceil(areaParede / 5);
            adicionaAltura.innerHTML = alturaParede;
            adicionaLargura.innerHTML = larguraParede;
            adicionarArea.innerHTML = areaParede;
            adicionarTinta.innerHTML = QntdTinta;        }
        else {
            if (janelaOuPorta == "janela") {
                if((areaJanela*2) <= areaParede){
                    var areaParede = areaParede - areaJanela;
                    var QntdTinta = Math.ceil(areaParede / 5);
                    adicionaAltura.innerHTML = alturaParede;
                    adicionaLargura.innerHTML = larguraParede;
                    adicionarArea.innerHTML = areaParede;
                    adicionarTinta.innerHTML = QntdTinta;
                }
            }
            else {
                if (janelaOuPorta == "porta") {
                    if (alturaParede >= 2.2 && ((areaPorta)* 2) <= areaParede){
                        var areaParede = areaParede - areaPorta;
                        var QntdTinta = Math.ceil(areaParede / 5);
                        adicionaAltura.innerHTML = alturaParede;
                        adicionaLargura.innerHTML = larguraParede;
                        adicionarArea.innerHTML = areaParede;
                        adicionarTinta.innerHTML = QntdTinta;
                    }
                    else{
                        document.getElementById("warning").classList.remove("none");
                        document.getElementById("ErrorMessage").innerHTML = "Dimensões das Janelas maiores que 50% do total";
                        return;
                    }
                }
                else {
                    if (janelaOuPorta == "janela e porta") {
                        if (alturaParede >= 2.2 && ((areaPorta * areaJanela )* 2) <= areaParede){
                            var areaParede = areaParede - areaPorta - areaJanela;
                            var QntdTinta = Math.ceil(areaParede / 5);
                            adicionaAltura.innerHTML = alturaParede;
                            adicionaLargura.innerHTML = larguraParede;
                            adicionarArea.innerHTML = areaParede;
                            adicionarTinta.innerHTML = QntdTinta;
                        }
                        else{
                            document.getElementById("warning").classList.remove("none");
                            document.getElementById("ErrorMessage").innerHTML = "Altura da Parede tem que ser no mínimo de 2,20m ou Dimensões das Janelas e portas maiores que 50% do total";
                            return;
                        }
                    }
                    else {
                        document.getElementById("warning").classList.remove("none");
                        document.getElementById("ErrorMessage").innerHTML = "Selecione se há portas ou janelas";
                        return;
                    }
                }
            }
        }
        while(QntdTinta !== 0){
            if (QntdTinta - 18 >= 0) {
                QntdTinta -= 18
                latasUsadas.litros18++
                document.getElementById("listaLatas18").classList.remove("none");
            }
    
            if (QntdTinta - 3.6 >= 0) {
                QntdTinta -= 3.6
                latasUsadas.litros3++
                document.getElementById("listaLatas3").classList.remove("none");
            }
    
            if (QntdTinta - 2.5 >= 0) {
                QntdTinta -= 2.5
                latasUsadas.litros2++
                document.getElementById("listaLatas2").classList.remove("none");
            }
    
            if (QntdTinta - 0.5 >= 0) {
                QntdTinta -= 0.5
                latasUsadas.litros0++
                document.getElementById("listaLatas0").classList.remove("none");
            }
        }
        document.getElementById("display").classList.remove("none");
        document.getElementById("latas18").innerHTML = latasUsadas.litros18;
        document.getElementById("latas3").innerHTML = latasUsadas.litros3;
        document.getElementById("latas2").innerHTML = latasUsadas.litros2;
        document.getElementById("latas0").innerHTML = latasUsadas.litros0;
        return latasUsadas    
    }
    else {
        document.getElementById("warning").classList.remove("none");
        document.getElementById("ErrorMessage").innerHTML = "Altura e Largura inválidas";
    }
}

function cancel() {
    document.getElementById("warning").classList.add("none")
}