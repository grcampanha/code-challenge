# Code Challenge

Inicie o arquivo "index.html" e informe as medidas da parede nos inputs que serão apresentados em sua tela!

Regras de Código
    Nenhuma parede pode ter menos de 1 metro quadrado nem mais de 15 metros quadrados, mas podem possuir alturas e larguras diferentes
    O total de área das portas e janelas deve ser no máximo 50% da área de parede
    A altura de paredes com porta deve ser, no mínimo, 30 centímetros maior que a altura da porta
    Cada janela possui as medidas: 2,00 x 1,20 mtos
    Cada porta possui as medidas: 0,80 x 1,90
    Cada litro de tinta é capaz de pintar 5 metros quadrados
    Não considerar teto nem piso.
    As variações de tamanho das latas de tinta são:
    0,5 L
    2,5 L
    3,6 L
    18 L